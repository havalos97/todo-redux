import React from "react";
import * as actions from "./redux/actions";
import { connect } from "react-redux";
import { List, ListItem, Checkbox, ListItemText, ListItemSecondaryAction, IconButton } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";

const TodoList = ({ todos, deleteTodo, toggleCompleted }) => {
	return (
		<List>
			{
				todos.map((task, index) => {
					return (
						<ListItem key={index}>
							<Checkbox checked={ task.completed } onClick={() => { toggleCompleted(index); }}/>
							<ListItemText primary={ task.value } />
							<ListItemSecondaryAction>
								<IconButton onClick={() => { deleteTodo(index); }}>
									<DeleteIcon />
								</IconButton>
							</ListItemSecondaryAction>
						</ListItem>
					)
				})
			}
		</List>
	);
}

const mapStateToProps = ({ todoReducer }) => {
	const { todos } = todoReducer;
	return { todos };
}

export default connect(
	mapStateToProps,
	actions
)(TodoList);
