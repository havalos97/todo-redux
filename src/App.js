import React, { Fragment } from "react";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import TodoList from "./TodoList.js";
import TodoForm from "./TodoForm.js";

const App = ({ todos, deleteTodo, toggleCompleted }) => {
	return(
		<Fragment>
			<Typography variant= "h2" align="center" gutterBottom>
				React - To Do
			</Typography>
			<Grid container justify="center">
				<Grid item >
					<TodoForm/>
				</Grid>
			</Grid>
			<Grid container justify="center">
				<Grid item md={8}>
				{
					<TodoList
						todos={ todos }
						deleteTodo={ deleteTodo }
						toggleCompleted={ toggleCompleted }/>
				}
				</Grid>
			</Grid>
		</Fragment>
	)
}

export default App;
