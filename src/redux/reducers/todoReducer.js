import { UPDATE_VALUE, SAVE_TODO, DELETE_TODO, TOGGLE_TODO } from "../actions/actionTypes.js"

const INITIAL_STATE = {
	value: "",
	todos: []
}

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case UPDATE_VALUE:
			return {
				...state,
				value: action.payload
			}

		case SAVE_TODO:
			return (state.value.trim()) ? {
				...state,
				value: "",
				todos: [...state.todos, { value:state.value, completed:false }]
			} : state;

		case DELETE_TODO:
			return {
				...state,
				todos: state.todos.filter((_, i) => {
					return (i !== action.payload)
				})
			};

		case TOGGLE_TODO:
			return {
				...state,
				todos: state.todos.map((todo, i) => {
					return (i === action.payload) ? ({ ...todo, completed:!(todo.completed) }) : (todo);
				})
			};

		default:
			return state;
	}
}
